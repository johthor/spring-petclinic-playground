#!/usr/bin/env sh

docker container run --rm --user $(id -u):$(id -g) \
  -e GRADLE_USER_HOME='/project/.gradle' \
  --entrypoint /bin/bash \
  -v ${PWD}:/project \
  rdmueller/doctoolchain:latest \
  doctoolchain /project $@ \
  -PbuildDir=/project/target \
  -PinputPath=src/docs \
  -PmainConfigFile=.buildconfig/docToolchain.groovy \
  -PpdfThemeDir=pdfTheme && exit
