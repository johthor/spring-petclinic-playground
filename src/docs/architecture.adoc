// header file for arc42-template,
// including all help texts
//
// ====================================

= image:arc42-logo.png[arc42] Template
// toc-title definition MUST follow document title without blank line!
:toc-title: Inhaltsverzeichnis

//additional style for arc42 help callouts
ifdef::basebackend-html[]
++++
<style>
.arc42help {font-size:small; width: 14px; height: 16px; overflow: hidden; position: absolute; right: 0px; padding: 2px 0px 3px 2px;}
.arc42help::before {content: "?";}
.arc42help:hover {width:auto; height: auto; z-index: 100; padding: 10px;}
.arc42help:hover::before {content: "";}
@media print {
	.arc42help {display:hidden;}
}
</style>
++++
endif::basebackend-html[]

// configure DE settings for asciidoc
include::arc42/config.adoc[]


include::arc42/about-arc42.adoc[]

// horizontal line
***




// numbering from here on
:numbered:

<<<<
// 1. Anforderungen und Ziele
include::arc42/01_introduction_and_goals.adoc[]

//<<<<
// 2. Randbedingungen
//include::arc42/02_architecture_constraints.adoc[]

<<<<
// 3. Kontextabgrenzung
include::arc42/03_system_scope_and_context.adoc[]

<<<<
// 4. Lösungsstrategie
include::arc42/04_solution_strategy.adoc[]

<<<<
// 5. Bausteinsicht
include::arc42/05_building_block_view.adoc[]

//<<<<
// 6. Laufzeitsicht
//include::arc42/06_runtime_view.adoc[]

//<<<<
// 7. Verteilungssicht
//include::arc42/07_deployment_view.adoc[]

<<<<
// 8. Querschnittliche Konzepte
include::arc42/08_concepts.adoc[]

//<<<<
// 9. Entscheidungen
//include::arc42/09_design_decisions.adoc[]

//<<<<
// 10. Qualität...
//include::arc42/10_quality_scenarios.adoc[]

//<<<<
// 11. Risiken
//include::arc42/11_technical_risks.adoc[]

//<<<<
// 12. Glossar
//include::arc42/12_glossary.adoc[]
